import {CategoryAction, CategoryActionTypes} from "../../types/category";
import {Dispatch} from "redux";

export const fetchCategories = () => {
    return async (dispatch: Dispatch<CategoryAction>) => {
        try {
            dispatch({type: CategoryActionTypes.FETCH_CATEGORIES})

            fetch('https://api.thecatapi.com/v1/categories')
                .then(response => response.json())
                .then(data =>
                    {
                        console.log(data);
                        dispatch({type: CategoryActionTypes.FETCH_CATEGORIES_SUCCESS, payload: data})
                    } 
                );
        } catch (e) {
            dispatch({
                type: CategoryActionTypes.FETCH_CATEGORIES_ERROR,
                payload: 'Error'
            })
        }
    }
}
