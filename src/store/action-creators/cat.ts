import {CatAction, CatActionTypes} from "../../types/cat";
import {Dispatch} from "redux";

export const fetchCats = (categoryID: number | null, limit: number) => {
    return async (dispatch: Dispatch<CatAction>) => {
        try {
            dispatch({type: CatActionTypes.FETCH_CATS})

            fetch(`https://api.thecatapi.com/v1/images/search?limit=${limit}&page=1${categoryID ? `&category_ids=${categoryID}` : ''}`)
                .then(response => response.json())
                .then(data =>
                    {
                        console.log(data);
                        console.log(`https://api.thecatapi.com/v1/images/search?limit=${limit}&page=1${categoryID ? `&category_ids=${categoryID}` : ''}`)
                        dispatch({type: CatActionTypes.FETCH_CATS_SUCCESS, payload: data})
                    } 
                );
        } catch (e) {
            dispatch({
                type: CatActionTypes.FETCH_CATS_ERROR,
                payload: 'Error'
            })
        }
    }
}

export const loadMore = (categoryID: number | null, limit: number) => {
    return async (dispatch: Dispatch<CatAction>) => {
        try {
            dispatch({type: CatActionTypes.FETCH_CATS})

            fetch(`https://api.thecatapi.com/v1/images/search?limit=${limit}&page=1${categoryID ? `&category_ids=${categoryID}` : ''}`)
                .then(response => response.json())
                .then(data =>
                    {
                        console.log(data);
                        dispatch({type: CatActionTypes.LOAD_MORE, payload: data})
                    } 
                );
        } catch (e) {
            dispatch({
                type: CatActionTypes.FETCH_CATS_ERROR,
                payload: 'Error'
            })
        }
    }
}
