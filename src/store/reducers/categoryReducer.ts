import {CategoryAction, CategoryActionTypes, CategoryState} from "../../types/category";

const initialState: CategoryState = {
    categories: [],
    error: null
}

export const categoryReducer = (state = initialState, action: CategoryAction): CategoryState => {
    switch (action.type) {
        case CategoryActionTypes.FETCH_CATEGORIES:
            return {...state}
        case CategoryActionTypes.FETCH_CATEGORIES_SUCCESS:
            return {...state, categories: action.payload}
        case CategoryActionTypes.FETCH_CATEGORIES_ERROR:
            return {...state, error: action.payload}
        default:
            return state
    }
}
