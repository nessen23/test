import {combineReducers} from "redux";
import {catReducer} from "./catReducer";
import {categoryReducer} from "./categoryReducer";

export const rootReducer = combineReducers({
    cat: catReducer,
    category: categoryReducer,
})

export type RootState = ReturnType<typeof rootReducer>
