import {CatAction, CatActionTypes, CatState} from "../../types/cat";

const initialState: CatState = {
    cats: [],
    loading: false,
    error: null
}

export const catReducer = (state = initialState, action: CatAction): CatState => {
    switch (action.type) {
        case CatActionTypes.FETCH_CATS:
            return {...state, loading: true}
        case CatActionTypes.FETCH_CATS_SUCCESS:
            return {...state, loading: false, cats: action.payload}
        case CatActionTypes.FETCH_CATS_ERROR:
            return {...state, loading: false, error: action.payload}
        case CatActionTypes.LOAD_MORE:
            return {...state, loading: false, cats: state.cats.concat(action.payload)}
        default:
            return state
    }
}
