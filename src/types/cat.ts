export interface CatState {
    cats: any[];
    loading: boolean;
    error: null | string;
}

export enum CatActionTypes {
    FETCH_CATS = 'FETCH_CATS',
    FETCH_CATS_SUCCESS = 'FETCH_CATS_SUCCESS',
    FETCH_CATS_ERROR = 'FETCH_CATS_ERROR',
    LOAD_MORE = "LOAD_MORE"
}

interface FetchCatsAction {
    type: CatActionTypes.FETCH_CATS;
}

interface FetchCatsSuccessAction {
    type: CatActionTypes.FETCH_CATS_SUCCESS;
    payload: any[]
}

interface FetchCatsErrorAction {
    type: CatActionTypes.FETCH_CATS_ERROR;
    payload: string;
}

interface LoadMore {
    type: CatActionTypes.LOAD_MORE;
    payload: any[]
}
export type CatAction = FetchCatsAction | FetchCatsErrorAction | FetchCatsSuccessAction | LoadMore

