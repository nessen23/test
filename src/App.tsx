import React from 'react';
import Sidebar from "./components/Sidebar/Sidebar";
import Cats from "./components/Cats/Cats";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  useRoutes,
} from "react-router-dom";
import "./App.scss";

function App() {
  return (
    <Router>
      <div className="container">
        <Sidebar/>
        <Cats/>
      </div>
    </Router>
  );
}

export default App;
