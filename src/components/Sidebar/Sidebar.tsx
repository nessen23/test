import React, {useEffect} from 'react';
import {TypedUseSelectorHook, useSelector, useDispatch} from "react-redux";
import {RootState} from "../../store/reducers";
import {fetchCategories} from "../../store/action-creators/category";
import Category from './Category/Category';
import "./Sidebar.scss";

const Sidebar: React.FC = () => {
    const dispatch = useDispatch()
    const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;
    const {categories, error } = useTypedSelector(state => state.category);

    useEffect(() => {
        dispatch(fetchCategories())
    }, [])

    if (error) {
        return <h1>{error}</h1>
    }

    return (
        <div className="sidebar-container">
            <div className="title">
                <p>Categories</p>
            </div>
            {categories.map(category =>
                <Category key={category.id} name={category.name} categoryID={category.id} />
            )}
        </div>
    );
};

export default Sidebar;
