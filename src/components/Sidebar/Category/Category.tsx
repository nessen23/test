import React from 'react';
import { useNavigate, useLocation } from "react-router-dom";
import { useDispatch } from "react-redux";
import { fetchCats } from "../../../store/action-creators/cat";
import "./Category.scss";

type CategoryProps = {
    categoryID: number | null,
    name: string
}

const Category: React.FC<CategoryProps> = ({categoryID, name}) => {
    const navigate = useNavigate();
    const location = useLocation();
    const dispatch = useDispatch();

    const selectCategory = () => {
        dispatch(fetchCats(categoryID, 10));
        navigate(`/${name}`);
    }

    return (
        <div className={`category ${name == location.pathname.replace('/', '') ? 'selected' : ''}`} onClick={() => selectCategory()}><p>{name}</p></div>
    );
};

export default Category;
