import React, {useEffect} from 'react';
import {TypedUseSelectorHook, useSelector, useDispatch} from "react-redux";
import {  useLocation } from "react-router-dom";
import {RootState} from "../../store/reducers";
import {fetchCats, loadMore} from "../../store/action-creators/cat";
import Loader from "../Loader/Loader";
import "./Cats.scss";

const Cats: React.FC = () => {
    const location = useLocation();
    const category_name = location.pathname.replace('/', '');
    const dispatch = useDispatch();
    const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;
    const { categories } = useTypedSelector(state => state.category);
    const { cats, loading, error } = useTypedSelector(state => state.cat);
    
    useEffect(() => {
        let category = null;
        if(category_name && categories.length) {
            category = categories.filter((category) => category.name === category_name)[0].id;
        }
        dispatch(fetchCats(category, 10));
    }, [categories])

    const loadMoreCats = () => {
        let category = null;
        if(category_name) {
            category = categories.filter((category) => category.name === category_name)[0].id;
        }
        dispatch(loadMore(category, 10));
    }

    if (error) {
        return <h1>{error}</h1>
    }

    return (
        <>
            <div className="cats-container">
                <div className="cats">
                    {cats.map((cat, index) =>
                        <div className="cat" key={index.toString()}>
                            <img src={cat.url} alt="cat" />
                        </div>
                    )}
                </div>

                {loading && <Loader/>}

                {!loading && <button className="load-more" onClick={() => loadMoreCats()}>Load More</button>}
            </div>
        </>
    );
};

export default Cats;
